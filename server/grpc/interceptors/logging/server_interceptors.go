package logging

import "gitee.com/pearl-zz/go-manage-system-core/server/grpc/interceptors/logging/ctxlog"

var (
	// SystemField is used in every log statement made through grpc_zap. Can be overwritten before any initialization code.
	SystemField = ctxlog.NewFields("system", "grpc")

	// ServerField is used in every server-side log statement made through grpc_zap.Can be overwritten before initialization.
	ServerField = ctxlog.NewFields("span.kind", "server")
)
