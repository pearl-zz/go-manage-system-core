package ctxlog

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
)

type ctxMarker struct {
}

type ctxLogger struct {
	logger *logger.Helper
	fields map[string]interface{}
}

var (
	ctxMarkerKey = &ctxMarker{}
	nullLogger   = logger.NewHelper(logger.DefaultLogger)
)

// AddFields adds logger fields to the logger.
func AddFields(ctx context.Context, fields map[string]interface{}) {
	l, ok := ctx.Value(ctxMarkerKey).(*ctxLogger)
	if !ok || l == nil {
		return
	}
	for k, v := range fields {
		l.fields[k] = v
	}
}

func Extract(ctx context.Context) *logger.Helper {
	l, ok := ctx.Value(ctxMarkerKey).(*ctxLogger)
	if !ok || l == nil {
		return nullLogger
	}

	fields := TagsToFields(ctx)
	for k, v := range fields {
		fields[k] = v
	}
	return l.logger.WithFields(fields)
}

// TagsToFields transforms the Tags on the supplied context into logger fields.
func TagsToFields(ctx context.Context) map[string]interface{} {
	return grpc_ctxtags.Extract(ctx).Values()
}

func ToContext(ctx context.Context, logger *logger.Helper) context.Context {
	l := &ctxLogger{
		logger: logger,
	}
	return context.WithValue(ctx, ctxMarkerKey, l)
}

// Debug is equivalent to calling Debug on the logger.Logger in the context.
// It is a no-op if the context does not contain a logger.Logger.
func Debug(ctx context.Context, msg string, fields map[string]interface{}) {
	Extract(ctx).Fields(fields).Log(logger.DebugLevel, msg)
}

// Info is equivalent to calling Info on the logger.Logger in the context.
// It is a no-op if the context does not contain a logger.Logger.
func Info(ctx context.Context, msg string, fields map[string]interface{}) {
	Extract(ctx).Fields(fields).Log(logger.InfoLevel, msg)
}

// Warn is equivalent to call Warn on the logger.Logger in the context.
// It is a no-op if the context does not contain a logger.Logger.
func Warn(ctx context.Context, msg string, fields map[string]interface{}) {
	Extract(ctx).Fields(fields).Log(logger.WarnLevel, msg)
}

// Error is equivalent to calling Error on the logger.Logger in the context.
// It is a no-op if the context does not contain a logger.Logger.
func Error(ctx context.Context, msg string, fields map[string]interface{}) {
	Extract(ctx).Fields(fields).Log(logger.ErrorLevel, msg)
}
