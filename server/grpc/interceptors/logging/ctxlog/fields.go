package ctxlog

type Fields struct {
	value map[string]interface{}
}

func NewFields(key string, value interface{}) *Fields {
	f := &Fields{}
	f.Set(key, value)
	return f
}

func (f *Fields) Set(key string, value interface{}) {
	if f.value == nil {
		f.value = make(map[string]interface{})
	}
	f.value[key] = value
}

func (f *Fields) Values() map[string]interface{} {
	return f.value
}

func (f *Fields) Merge(r *Fields) {
	if len(r.value) > 0 {
		for k, v := range r.value {
			f.Set(k, v)
		}
	}
}
