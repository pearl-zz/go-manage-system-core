package server

import (
	"context"
	"fmt"
)

type Runnable interface {
	fmt.Stringer
	Start(ctx context.Context) error // 启动
	Attempt() bool                   // 是否允许自启
}

type Manager interface {
	Add(...Runnable)
	Start(ctx context.Context) error
}
