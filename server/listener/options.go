package listener

import "net/http"

type options struct {
	addr, certFile, keyFile string
	handler                 http.Handler
	startedHook             func()
	endHook                 func()
}

type Option func(*options)

func setDefaultOption() options {
	return options{
		addr: ":8080",
		handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		}),
	}
}

// WithAddr 设置addr
func WithAddr(s string) Option {
	return func(o *options) {
		o.addr = s
	}
}

// WithCert 设置cert
func WithCert(s string) Option {
	return func(o *options) {
		o.certFile = s
	}
}

// WithKey 设置key
func WithKey(s string) Option {
	return func(o *options) {
		o.keyFile = s
	}
}

// WithHandler 设置handler
func WithHandler(handler http.Handler) Option {
	return func(o *options) {
		o.handler = handler
	}
}

// WithStartedHook 设置启动回调函数
func WithStartedHook(f func()) Option {
	return func(o *options) {
		o.startedHook = f
	}
}

func WithEndHook(f func()) Option {
	return func(o *options) {
		o.endHook = f
	}
}
