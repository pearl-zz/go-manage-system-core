package server

import "time"

type options struct {
	gracefulShutdownTimeout time.Duration
}

type Option func(*options)

func setDefaultOptions() options {
	return options{
		gracefulShutdownTimeout: time.Second * 5,
	}
}
