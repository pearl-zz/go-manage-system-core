package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"testing"
)

var (
	dsn0   = "root:123456@tcp(127.0.0.1:3306)/mysql?charset=utf8mb4&parseTime=True"
	dsn1   = "root:123456@tcp(127.0.0.1:3306)/mysql?charset=utf8mb4&parseTime=True"
	tables = []interface{}{"sys_user", "sys_role"}
)

func TestDBConfig_Init(t *testing.T) {
	type fields struct {
		dsn             string
		connMaxIdleTime int // 最大空闲时间,单位秒
		connMaxLifetime int // 存活时间,单位秒
		maxIdleConn     int
		maxOpenConn     int
		registers       []ResolverConfigure
	}

	type args struct {
		config *gorm.Config
		open   func(string) gorm.Dialector
	}

	registers := make([]ResolverConfigure, 0)
	registers = append(registers, &DBResolverConfig{
		sources:  []string{dsn0},
		replicas: []string{dsn1},
		policy:   "random",
		tables:   tables,
	})

	registers = append(registers, &DBResolverConfig{
		sources:  []string{dsn0},
		replicas: []string{dsn1},
		policy:   "random",
	})

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test01",
			fields{
				dsn: dsn0,
			},
			args{
				config: &gorm.Config{},
				open:   mysql.Open,
			},
			false,
		},
		{
			"test02",
			fields{
				dsn:             dsn0,
				connMaxLifetime: 600,
				connMaxIdleTime: 600,
				maxIdleConn:     200,
				maxOpenConn:     100,
				registers:       registers,
			},
			args{
				config: &gorm.Config{},
				open:   mysql.Open,
			},
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := &DBConfig{
				dsn:             test.fields.dsn,
				connMaxIdleTime: test.fields.connMaxIdleTime,
				connMaxLifetime: test.fields.connMaxLifetime,
				maxIdleConn:     test.fields.maxIdleConn,
				maxOpenConn:     test.fields.maxOpenConn,
				registers:       test.fields.registers,
			}
			_, err := e.Init(test.args.config, test.args.open)

			if (err != nil) != test.wantErr {
				t.Errorf("Init() err = %v, wantErr %v", err, test.wantErr)
				return
			}
		})
	}
}
