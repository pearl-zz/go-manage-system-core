package logger

import (
	"context"
	loggerCore "gitee.com/pearl-zz/go-manage-system-core/logger"
	"gorm.io/gorm/logger"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	l := New(logger.Config{
		SlowThreshold: time.Second,
		Colorful:      true,
		LogLevel:      logger.LogLevel(loggerCore.DefaultLogger.Options().Level.LevelForGorm()),
	})

	l.Info(context.TODO(), "test")
}
