package poster

import (
	"bufio"
	"image/png"
	"os"
	"testing"
)

func TestGetImage(t *testing.T) {
	src := "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F611%2F031213123016%2F130312123016-3-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1642487219&t=239694c8988de6b83b55d9395cd90baa"
	img, err := GetImage(src)
	if err != nil {
		t.Error(err)
	}
	outFile, err := os.Create("风景.png")
	defer outFile.Close()
	if err != nil {
		t.Error(err)
	}

	b := bufio.NewWriter(outFile)
	err = png.Encode(b, img)
	if err != nil {
		t.Error(err)
	}

	err = b.Flush()
	if err != nil {
		t.Error(err)
	}
}
