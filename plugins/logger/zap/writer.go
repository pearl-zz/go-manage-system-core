package zap

import (
	"github.com/natefinch/lumberjack"
	"io"
)

// getWriter 使用lumberjack写入文件
func getWriter(filename string) io.Writer {
	return &lumberjack.Logger{
		Filename:   filename,
		MaxSize:    20,    //最大M数，超过则切割
		MaxBackups: 10,    //最大文件保留数，超过就删除最老的日志文件
		MaxAge:     30,    //保存30天
		Compress:   false, //是否压缩
	}
}
