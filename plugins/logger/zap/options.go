package zap

import (
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
)

type callerSkipKey struct {
}

func WithCallerSkip(i int) logger.Option {
	return logger.SetOption(callerSkipKey{}, i)
}

type configKey struct {
}

// WithConfig 配置zap.config
func WithConfig(c zap.Config) logger.Option {
	return logger.SetOption(configKey{}, c)
}

type encoderConfigKey struct {
}

// WithEncoderConfig 配置EncoderConfig
func WithEncoderConfig(c zapcore.EncoderConfig) logger.Option {
	return logger.SetOption(encoderConfigKey{}, c)
}

type writeKey struct {
}

// WithOutput 配置文件输出路径
func WithOutput(out io.Writer) logger.Option {
	return logger.SetOption(writeKey{}, out)
}

type infoPathKey struct {
}

// WithInfoPath 配置info日志地址,级别为Info
func WithInfoPath(infoPath string) logger.Option {
	return logger.SetOption(infoPathKey{}, infoPath)
}

type errPathKey struct {
}

// WithErrPath 配置错误日志地址，级别为Warn及以上
func WithErrPath(errPath string) logger.Option {
	return logger.SetOption(errPathKey{}, errPath)
}
