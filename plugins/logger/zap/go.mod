module gitee.com/pearl-zz/go-manage-system-core/plugins/logger/zap

go 1.15

require (
	gitee.com/pearl-zz/go-manage-system-core v1.0.3
	github.com/natefinch/lumberjack v2.0.0+incompatible
	go.uber.org/zap v1.10.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)

replace gitee.com/pearl-zz/go-manage-system-core => ../../../
