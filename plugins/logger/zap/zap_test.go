package zap

import (
	"gitee.com/pearl-zz/go-manage-system-core/debug/writer"
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"testing"
	"time"
)

func TestName(t *testing.T) {
	l, err := NewLogger()
	if err != nil {
		t.Fatal(err)
	}

	if l.String() != "zap" {
		t.Errorf("name is error: %s", l.String())
	}

	t.Logf("test logger name: %s", l.String())
}

func TestWithConfig(t *testing.T) {
	config := zap.NewProductionConfig()
	l, err := NewLogger(WithConfig(config))
	if err != nil {
		t.Fatal(err)
	}
	l.Log(logger.InfoLevel, "test")
}

func TestWithEncoderConfig(t *testing.T) {
	config := zapcore.EncoderConfig{
		MessageKey:   "msg",                       //结构化（json）输出：msg的key
		LevelKey:     "level",                     //结构化（json）输出：日志级别的key（INFO，WARN，ERROR等）
		TimeKey:      "timestamp",                 //结构化（json）输出：时间的key
		CallerKey:    "file",                      //结构化（json）输出：打印日志的文件对应的Key
		EncodeLevel:  zapcore.CapitalLevelEncoder, //将日志级别转换成大写（INFO，WARN，ERROR等）
		EncodeCaller: zapcore.ShortCallerEncoder,  //采用短文件路径编码输出（test/main.go:14 ）
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05"))
		}, //输出的时间格式
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendInt64(int64(d) / 1000000)
		},
	}
	l, err := NewLogger(WithEncoderConfig(config))
	if err != nil {
		t.Fatal(err)
	}
	l.Log(logger.InfoLevel, "test")
}

func TestWithLevel(t *testing.T) {
	l, err := NewLogger(logger.WithLevel(logger.DebugLevel))
	if err != nil {
		t.Fatal(err)
	}
	logger.DefaultLogger = l
	l.Logf(logger.DebugLevel, "test show debug: %s", "debug msg")
	l.Logf(logger.WarnLevel, "test show debug: %s", "warn msg")

	logger.Init(logger.WithLevel(logger.InfoLevel))
	l.Logf(logger.DebugLevel, "test non-show debug: %s", "debug msg")
}

func TestWithReportCaller(t *testing.T) {
	l, err := NewLogger(WithCallerSkip(0))
	if err != nil {
		t.Fatal(err)
	}
	logger.DefaultLogger = l
	logger.Logf(logger.InfoLevel, "testing: %s", "WithReportCaller")
}

func TestWithField(t *testing.T) {
	l, err := NewLogger(logger.WithFields(map[string]interface{}{
		"x-request-id": "123",
	}))
	if err != nil {
		t.Fatal(err)
	}
	l.Log(logger.InfoLevel, "method 1")

	logger.DefaultLogger = l.Fields(map[string]interface{}{
		"address": "127.0.0.1",
	})
	logger.DefaultLogger.Log(logger.InfoLevel, "method 2")
}

func TestWithInfoPath(t *testing.T) {
	l, err := NewLogger(logger.WithLevel(logger.TraceLevel), WithInfoPath("./info.log"))
	if err != nil {
		t.Fatal(err)
	}
	l.Log(logger.TraceLevel, "trace level will be recorded")
	l.Log(logger.InfoLevel, "info level will be recorded")
	l.Log(logger.WarnLevel, "warn level will not be recorded")
}

func TestWithErrPath(t *testing.T) {
	l, err := NewLogger(logger.WithLevel(logger.InfoLevel), WithErrPath("./err.log"))
	if err != nil {
		t.Fatal(err)
	}
	l.Log(logger.InfoLevel, "info level will not be recorded")
	l.Log(logger.WarnLevel, "warn level will be recorded")
	l.Log(logger.ErrorLevel, "error level will be recorded")
}

func TestWithOutput(t *testing.T) {
	output, err := writer.NewFileWriter(writer.WithPath(os.TempDir()))
	if err != nil {
		t.Errorf("logger setup error: %s", err.Error())
	}

	// var err error
	logger.DefaultLogger, err = NewLogger(logger.WithLevel(logger.TraceLevel), WithOutput(output))
	if err != nil {
		t.Errorf("logger setup error: %s", err.Error())
	}

	logger.DefaultLogger = logger.DefaultLogger.Fields(map[string]interface{}{
		"x-request-id": "1234",
	})

	t.Log(logger.DefaultLogger)
	logger.DefaultLogger.Log(logger.WarnLevel, "hello")
}
