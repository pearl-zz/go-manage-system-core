package logrus

import (
	"errors"
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"github.com/sirupsen/logrus"
	"os"
	"testing"
)

func TestWithFields(t *testing.T) {
	l := NewLogger(logger.WithOutput(os.Stdout)).Fields(map[string]interface{}{
		"k1": "v1",
		"k2": 123456,
	})
	logger.DefaultLogger = l
	logger.Log(logger.InfoLevel, "testing: Info")
	logger.Logf(logger.InfoLevel, "testing: %s", "infof")
}

func TestWithError(t *testing.T) {
	l := NewLogger().Fields(map[string]interface{}{"error": errors.New("boom")})
	logger.DefaultLogger = l
	logger.Log(logger.InfoLevel, "testing: error")
}

func TestWithLogger(t *testing.T) {
	l := NewLogger(WithLogger(logrus.StandardLogger())).Fields(map[string]interface{}{
		"k1": "v1",
		"k2": 123456,
	})
	logger.DefaultLogger = l
	logger.Log(logger.InfoLevel, "testing: with *logrus.Logger")

	// with *logrus.Entry
	el := NewLogger(WithLogger(logrus.NewEntry(logrus.StandardLogger()))).Fields(map[string]interface{}{
		"k3": 3.456,
		"k4": true,
	})
	logger.DefaultLogger = el
	logger.Log(logger.InfoLevel, "testing: with *logrus.Entry")
}

func TestWithJSONFormatter(t *testing.T) {
	logger.DefaultLogger = NewLogger(WithJSONFormatter(&logrus.JSONFormatter{}))

	logger.Logf(logger.InfoLevel, "test logf: %s", "name")
}

func TestSetLevel(t *testing.T) {
	logger.DefaultLogger = NewLogger()
	logger.Init(logger.WithLevel(logger.DebugLevel))
	logger.Logf(logger.DebugLevel, "test show debug: %s", "debug msg")

	logger.Init(logger.WithLevel(logger.InfoLevel))
	logger.Logf(logger.DebugLevel, "test non-show debug: %s", "debug msg")
}

func TestWithReportCaller(t *testing.T) {
	logger.DefaultLogger = NewLogger(ReportCaller())
	logger.Logf(logger.InfoLevel, "testing: %s", "WithReportCaller")
}
