module gitee.com/pearl-zz/go-manage-system-core/plugins/logger/logrus

go 1.15

require (
	gitee.com/pearl-zz/go-manage-system-core v1.0.3
	github.com/sirupsen/logrus v1.8.1
)

replace gitee.com/pearl-zz/go-manage-system-core => ../../../
