package logrus

import (
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"github.com/sirupsen/logrus"
)

type Options struct {
	logger.Options
	Formatter    logrus.Formatter
	Hooks        logrus.LevelHooks
	ReportCaller bool      // Flag for whether to log caller info (off by default)
	ExitFunc     func(int) // Exit function to call when fatalLevel log
}

type formatterKey struct {
}

func WithTextFormatter(formatter *logrus.TextFormatter) logger.Option {
	return logger.SetOption(formatterKey{}, formatter)
}

func WithJSONFormatter(formatter *logrus.JSONFormatter) logger.Option {
	return logger.SetOption(formatterKey{}, formatter)
}

type hooksKey struct {
}

func WithLevelHooks(hooks logrus.LevelHooks) logger.Option {
	return logger.SetOption(hooksKey{}, hooks)
}

type reportCallerKey struct {
}

// ReportCaller warning to use the option. because logrus doest not open CallerDepth option
// thi will only print this package
func ReportCaller() logger.Option {
	return logger.SetOption(reportCallerKey{}, true)
}

type exitKey struct {
}

func WithExitFunc(exit func(int)) logger.Option {
	return logger.SetOption(exitKey{}, exit)
}

type logrusLoggerKey struct {
}

func WithLogger(l logrus.StdLogger) logger.Option {
	return logger.SetOption(logrusLoggerKey{}, l)
}
