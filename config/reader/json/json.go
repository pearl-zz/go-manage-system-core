package json

import (
	"errors"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/json"
	"gitee.com/pearl-zz/go-manage-system-core/config/reader"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"github.com/imdario/mergo"
	"time"
)

const readerTyp = "json"

type jsonReader struct {
	opts reader.Options
	json encoder.Encoder
}

// NewReader create a json reader
func NewReader(opts ...reader.Option) reader.Reader {
	options := reader.NewOptions(opts...)
	return &jsonReader{
		opts: options,
		json: json.NewEncoder(),
	}
}

func (j *jsonReader) Merge(changeSets ...*source.ChangeSet) (*source.ChangeSet, error) {
	var merged map[string]interface{}

	for _, m := range changeSets {
		if m == nil {
			continue
		}

		if len(m.Data) == 0 {
			continue
		}

		codec, ok := j.opts.Encoding[m.Format]
		if !ok {
			codec = j.json
		}

		var data map[string]interface{}
		if err := codec.Decode(m.Data, &data); err != nil {
			return nil, err
		}

		if err := mergo.Map(&merged, data, mergo.WithOverride); err != nil {
			return nil, err
		}
	}

	b, err := j.json.Encode(merged)
	if err != nil {
		return nil, err
	}

	cs := &source.ChangeSet{
		Data:      b,
		Format:    j.json.String(),
		Source:    readerTyp,
		Timestamp: time.Now(),
	}
	cs.CheckSum = cs.Sum()
	return cs, nil
}

func (j *jsonReader) Values(changeSet *source.ChangeSet) (reader.Values, error) {
	if changeSet == nil {
		return nil, errors.New("changeSet is nil")
	}
	if changeSet.Format != "json" {
		return nil, errors.New("unsupported format")
	}
	return newValues(changeSet)
}

func (j *jsonReader) String() string {
	return readerTyp
}
