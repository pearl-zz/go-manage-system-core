package reader

import (
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/json"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/toml"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/xml"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/yaml"
)

type Options struct {
	Encoding map[string]encoder.Encoder
}

type Option func(o *Options)

func NewOptions(opts ...Option) Options {
	options := Options{
		Encoding: map[string]encoder.Encoder{
			"json": json.NewEncoder(),
			"xml":  xml.NewEncoder(),
			"yml":  yaml.NewEncoder(),
			"yaml": yaml.NewEncoder(),
			"toml": toml.NewEncoder(),
		}}

	for _, o := range opts {
		o(&options)
	}
	return options
}

func WithEncoder(e encoder.Encoder) Option {
	return func(o *Options) {
		if o.Encoding == nil {
			o.Encoding = make(map[string]encoder.Encoder)
		}
		o.Encoding[e.String()] = e
	}
}
