// Package loader manager loading from multiple sources
package loader

import "gitee.com/pearl-zz/go-manage-system-core/config/source"

// Loader manages loading sources
type Loader interface {
	Load(...source.Source) error      // Load the sources
	Snapshot() (*Snapshot, error)     // Snapshot A Snapshot of loaded config
	Sync() error                      // Sync Force sync of sources
	Watch(...string) (Watcher, error) // Watch for changes
	Close() error                     // Close stop the loader
	String() string                   // String name of loader
}

// Snapshot is a merged ChangeSet
type Snapshot struct {
	// The merged ChangeSet
	ChangeSet *source.ChangeSet
	Version   string
}

func Copy(s *Snapshot) *Snapshot {
	cs := *(s.ChangeSet)
	return &Snapshot{
		ChangeSet: &cs,
		Version:   s.Version,
	}
}

// Watcher lets you watch sources and returns a merged ChangeSet
type Watcher interface {
	Next() (*Snapshot, error)
	Stop() error
}
