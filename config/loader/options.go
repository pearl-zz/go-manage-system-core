package loader

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/config/reader"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
)

type Options struct {
	Reader  reader.Reader
	Source  []source.Source
	Context context.Context
}

type Option func(options *Options)

// WithSource appends a source to list of sources
func WithSource(s source.Source) Option {
	return func(o *Options) {
		o.Source = append(o.Source, s)
	}
}

// WithReader sets the config reader
func WithReader(r reader.Reader) Option {
	return func(o *Options) {
		o.Reader = r
	}
}
