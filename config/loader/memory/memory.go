package memory

import (
	"bytes"
	"container/list"
	"errors"
	"fmt"
	"gitee.com/pearl-zz/go-manage-system-core/config/loader"
	"gitee.com/pearl-zz/go-manage-system-core/config/reader"
	"gitee.com/pearl-zz/go-manage-system-core/config/reader/json"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"strings"
	"sync"
	"time"
)

type memory struct {
	exit chan bool
	sync.RWMutex
	opts     loader.Options
	snap     *loader.Snapshot
	values   reader.Values
	sets     []*source.ChangeSet
	sources  []source.Source
	watchers *list.List
}

func NewLoader(opts ...loader.Option) loader.Loader {
	options := loader.Options{
		Reader: json.NewReader(),
	}
	for _, o := range opts {
		o(&options)
	}

	m := &memory{
		exit:     make(chan bool),
		opts:     options,
		watchers: list.New(),
		sources:  options.Source,
		sets:     make([]*source.ChangeSet, len(options.Source)),
	}

	for i, s := range options.Source {
		m.sets[i] = &source.ChangeSet{Source: s.String()}
		go m.watch(i, s)
	}
	return m
}

func (m *memory) Load(sources ...source.Source) error {
	var gErrors []string
	for _, s := range sources {
		set, err := s.Read()
		if err != nil {
			gErrors = append(gErrors, fmt.Sprintf("error loading source %s:%v", s, err))
			continue
		}

		m.Lock()
		m.sources = append(m.sources, s)
		m.sets = append(m.sets, set)
		idx := len(m.sets) - 1
		m.Unlock()

		go m.watch(idx, s)
	}

	if err := m.reload(); err != nil {
		gErrors = append(gErrors, err.Error())
	}

	if len(gErrors) != 0 {
		return errors.New(strings.Join(gErrors, "\n"))
	}
	return nil
}

func (m *memory) Snapshot() (*loader.Snapshot, error) {
	if m.loaded() {
		m.RLock()
		snap := loader.Copy(m.snap)
		m.RUnlock()
		return snap, nil
	}

	// not loaded, sync
	if err := m.Sync(); err != nil {
		return nil, err
	}

	// make copy
	m.RLock()
	snap := loader.Copy(m.snap)
	m.RUnlock()
	return snap, nil
}

func (m *memory) Sync() error {
	var sets []*source.ChangeSet
	m.Lock()
	var gError []string
	for _, s := range m.sources {
		cs, err := s.Read()
		if err != nil {
			gError = append(gError, err.Error())
			continue
		}
		sets = append(sets, cs)
	}

	// merge sets
	set, err := m.opts.Reader.Merge(sets...)
	if err != nil {
		m.Unlock()
		return err
	}

	// set values
	vals, err := m.opts.Reader.Values(set)
	if err != nil {
		m.Unlock()
		return err
	}

	m.values = vals
	m.snap = &loader.Snapshot{
		ChangeSet: set,
		Version:   genVer(),
	}
	m.Unlock()

	m.update()

	if len(gError) > 0 {
		return fmt.Errorf("source loading errors: %s", strings.Join(gError, "\n"))
	}
	return nil
}

func (m *memory) Watch(path ...string) (loader.Watcher, error) {
	value, err := m.Get(path...)
	if err != nil {
		return nil, err
	}

	m.Lock()
	w := &watcher{
		exit:    make(chan bool),
		path:    path,
		value:   value,
		reader:  m.opts.Reader,
		version: m.snap.Version,
		updates: make(chan updateValue, 1),
	}
	e := m.watchers.PushBack(w)
	m.Unlock()

	go func() {
		<-w.exit
		m.Lock()
		m.watchers.Remove(e)
		m.Unlock()
	}()

	return w, nil
}

func (m *memory) Close() error {
	select {
	case <-m.exit:
		return nil
	default:
		close(m.exit)
	}
	return nil
}

func (m *memory) String() string {
	return "memory"
}

func (m *memory) Get(path ...string) (reader.Value, error) {
	if !m.loaded() {
		if err := m.Sync(); err != nil {
			return nil, err
		}
	}

	m.Lock()
	defer m.Unlock()

	if m.values != nil {
		return m.values.Get(path...), nil
	}

	ch := m.snap.ChangeSet
	v, err := m.opts.Reader.Values(ch)
	if err != nil {
		return nil, err
	}

	m.values = v
	if m.values != nil {
		return m.values.Get(path...), nil
	}

	return nil, errors.New("no values")
}

func (m *memory) watch(idx int, s source.Source) {
	// watches a source for changes
	watch := func(idx int, s source.Watcher) error {
		for {
			cs, err := s.Next()
			if err != nil {
				return err
			}

			m.Lock()

			// save
			m.sets[idx] = cs

			// merge sets
			set, err := m.opts.Reader.Merge(m.sets...)
			if err != nil {
				m.Unlock()
				return err
			}

			// set values
			m.values, _ = m.opts.Reader.Values(set)
			m.snap = &loader.Snapshot{
				ChangeSet: set,
				Version:   genVer(),
			}

			m.Unlock()

			// send watch updates
			m.update()
		}
	}

	for {
		// watch the source
		w, err := s.Watch()
		if err != nil {
			time.Sleep(time.Second)
			continue
		}

		done := make(chan bool)

		// the stop watch func
		go func() {
			select {
			case <-done:
			case <-m.exit:
			}
			_ = w.Stop()
		}()

		// block watch
		if err := watch(idx, w); err != nil {
			// do something better
			time.Sleep(time.Second)
		}

		// close done chan
		close(done)

		// if the config is closed exit
		select {
		case <-m.exit:
			return
		default:
		}
	}
}

func (m *memory) loaded() bool {
	var loaded bool
	m.RLock()
	if m.values != nil {
		loaded = true
	}
	m.RUnlock()
	return loaded
}

func (m *memory) reload() error {
	m.Lock()

	// merge sets
	set, err := m.opts.Reader.Merge(m.sets...)
	if err != nil {
		m.Unlock()
		return err
	}

	// set values
	m.values, _ = m.opts.Reader.Values(set)
	m.snap = &loader.Snapshot{
		ChangeSet: set,
		Version:   genVer(),
	}

	m.Unlock()

	// update watchers
	m.update()

	return nil
}

// update watchers
func (m *memory) update() {
	watchers := make([]*watcher, 0, m.watchers.Len())
	m.RLock()

	for e := m.watchers.Front(); e != nil; e = e.Next() {
		watchers = append(watchers, e.Value.(*watcher))
	}

	vals := m.values
	snap := m.snap

	m.RUnlock()

	for _, w := range watchers {
		if w.version >= snap.Version {
			continue
		}

		uv := updateValue{
			value:   vals.Get(w.path...),
			version: m.snap.Version,
		}

		select {
		case w.updates <- uv:
		default:

		}
	}
}

func genVer() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

type updateValue struct {
	value   reader.Value
	version string
}

type watcher struct {
	exit    chan bool
	path    []string
	value   reader.Value
	reader  reader.Reader
	version string
	updates chan updateValue
}

func (w *watcher) Next() (*loader.Snapshot, error) {
	update := func(v reader.Value) *loader.Snapshot {
		w.value = v
		cs := &source.ChangeSet{
			Data:      v.Bytes(),
			CheckSum:  "",
			Format:    w.reader.String(),
			Source:    "memory",
			Timestamp: time.Now(),
		}
		cs.CheckSum = cs.Sum()

		return &loader.Snapshot{
			ChangeSet: cs,
			Version:   w.version,
		}
	}

	for {
		select {
		case <-w.exit:
			return nil, errors.New("watcher stopped")
		case uv := <-w.updates:
			if uv.version <= w.version {
				continue
			}
			v := uv.value

			w.version = uv.version
			if bytes.Equal(w.value.Bytes(), v.Bytes()) {
				continue
			}

			return update(v), nil
		}
	}
}

func (w *watcher) Stop() error {
	select {
	case <-w.exit:
	default:
		close(w.exit)
		close(w.updates)
	}
	return nil
}
