package xml

import (
	"encoding/xml"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
)

type xmlEncoder struct {
}

func NewEncoder() encoder.Encoder {
	return xmlEncoder{}
}

func (x xmlEncoder) Encode(v interface{}) ([]byte, error) {
	return xml.Marshal(v)
}

func (x xmlEncoder) Decode(data []byte, v interface{}) error {
	return xml.Unmarshal(data, v)
}

func (x xmlEncoder) String() string {
	return "xml"
}
