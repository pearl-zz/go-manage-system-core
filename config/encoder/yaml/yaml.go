package yaml

import (
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
	"github.com/ghodss/yaml"
)

func NewEncoder() encoder.Encoder {
	return yamlEncoder{}
}

type yamlEncoder struct {
}

func (y yamlEncoder) Encode(v interface{}) ([]byte, error) {
	return yaml.Marshal(v)
}

func (y yamlEncoder) Decode(data []byte, v interface{}) error {
	return yaml.Unmarshal(data, v)
}

func (y yamlEncoder) String() string {
	return "yaml"
}
