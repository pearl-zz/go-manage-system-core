package toml

import (
	"bytes"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
	"github.com/BurntSushi/toml"
)

type tomlEncoder struct {
}

func NewEncoder() encoder.Encoder {
	return tomlEncoder{}
}

func (t tomlEncoder) Encode(v interface{}) ([]byte, error) {
	b := bytes.NewBuffer(nil)
	defer b.Reset()
	err := toml.NewEncoder(b).Encode(v)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (t tomlEncoder) Decode(data []byte, v interface{}) error {
	return toml.Unmarshal(data, v)
}

func (t tomlEncoder) String() string {
	return "toml"
}
