package json

import (
	"encoding/json"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
)

type jsonEncoder struct {
}

func NewEncoder() encoder.Encoder {
	return jsonEncoder{}
}

func (j jsonEncoder) Encode(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (j jsonEncoder) Decode(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (j jsonEncoder) String() string {
	return "json"
}
