package box

import (
	"crypto/rand"
	"gitee.com/pearl-zz/go-manage-system-core/config/secrets"
	"github.com/pkg/errors"
	naclbox "golang.org/x/crypto/nacl/box"
)

const keyLength = 32

type box struct {
	options    secrets.Options
	publicKey  [keyLength]byte
	privateKey [keyLength]byte
}

func NewSecrets(opts ...secrets.Option) secrets.Secrets {
	b := &box{}
	for _, o := range opts {
		o(&b.options)
	}
	return b
}

func (b *box) Init(opts ...secrets.Option) error {
	for _, o := range opts {
		o(&b.options)
	}
	if len(b.options.PrivateKey) != keyLength || len(b.options.PublicKey) != keyLength {
		return errors.Errorf("a public key an a private key of length %d must both be provided", keyLength)
	}

	copy(b.publicKey[:], b.options.PublicKey)
	copy(b.privateKey[:], b.options.PrivateKey)
	return nil
}

func (b *box) Options() secrets.Options {
	return b.options
}

func (b *box) Encrypt(data []byte, opts ...secrets.EncryptOption) ([]byte, error) {
	var options secrets.EncryptOptions
	for _, o := range opts {
		o(&options)
	}

	if len(options.RecipientPublicKey) != keyLength {
		return []byte{}, errors.New("Recipient's public key must be provided")
	}
	var recipientPublicKey [keyLength]byte
	copy(recipientPublicKey[:], options.RecipientPublicKey)
	var nonce [24]byte

	if _, err := rand.Read(nonce[:]); err != nil {
		return []byte{}, errors.Wrap(err, "couldn't obtain a random nonce from crypto/rand")
	}
	return naclbox.Seal(nonce[:], data, &nonce, &recipientPublicKey, &b.privateKey), nil
}

func (b *box) Decrypt(data []byte, opts ...secrets.DecryptOption) ([]byte, error) {
	var options secrets.DecryptOptions
	for _, o := range opts {
		o(&options)
	}

	if len(options.SenderPublicKey) != keyLength {
		return []byte{}, errors.New("sender's public key bust be provided")
	}

	var nonce [24]byte
	var senderPublicKey [32]byte
	copy(nonce[:], data[:24])
	copy(senderPublicKey[:], options.SenderPublicKey)
	decrypted, ok := naclbox.Open(nil, data[24:], &nonce, &senderPublicKey, &b.privateKey)
	if !ok {
		return []byte{}, errors.New("incoming message couldn't be verified / decrypted")
	}
	return decrypted, nil
}

func (b *box) String() string {
	return "nacl-box"
}
