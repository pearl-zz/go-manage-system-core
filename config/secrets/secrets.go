package secrets

import "context"

type Secrets interface {
	Init(...Option) error
	Options() Options
	Encrypt([]byte, ...EncryptOption) ([]byte, error)
	Decrypt([]byte, ...DecryptOption) ([]byte, error)
	String() string
}

type Options struct {
	Key        []byte
	PrivateKey []byte
	PublicKey  []byte
	Context    context.Context
}

type Option func(options *Options)

func Key(key []byte) Option {
	return func(o *Options) {
		o.Key = make([]byte, len(key))
		copy(o.Key, key)
	}
}

func PrivateKey(key []byte) Option {
	return func(o *Options) {
		o.PrivateKey = make([]byte, len(key))
		copy(o.PrivateKey, key)
	}
}

func PublicKey(key []byte) Option {
	return func(o *Options) {
		o.PublicKey = make([]byte, len(key))
		copy(o.PublicKey, key)
	}
}

type DecryptOptions struct {
	SenderPublicKey []byte
}

type DecryptOption func(options *DecryptOptions)

func SenderPublicKey(key []byte) DecryptOption {
	return func(o *DecryptOptions) {
		o.SenderPublicKey = make([]byte, len(key))
		copy(o.SenderPublicKey, key)
	}
}

type EncryptOptions struct {
	RecipientPublicKey []byte
}

type EncryptOption func(options *EncryptOptions)

func RecipientPublicKey(key []byte) EncryptOption {
	return func(o *EncryptOptions) {
		o.RecipientPublicKey = make([]byte, len(key))
		copy(o.RecipientPublicKey, key)
	}
}
