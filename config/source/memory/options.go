package memory

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
)

type changeSetKey struct {
}

func WithData(d []byte, format string) source.Option {
	return func(o *source.Options) {
		if o.Context == nil {
			o.Context = context.Background()
		}
		o.Context = context.WithValue(o.Context, changeSetKey{}, &source.ChangeSet{
			Data:   d,
			Format: format,
		})
	}
}

func WithJSON(d []byte) source.Option {
	return WithData(d, "json")
}

func WithYAML(d []byte) source.Option {
	return WithData(d, "yaml")
}

func WithChangeSet(cs *source.ChangeSet) source.Option {
	return func(o *source.Options) {
		if o.Context == nil {
			o.Context = context.Background()
		}
		o.Context = context.WithValue(o.Context, changeSetKey{}, cs)
	}
}
