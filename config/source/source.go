package source

import (
	"errors"
	"time"
)

var (
	ErrWatcherStopped = errors.New("watcher stopped")
)

// Source is the source from which config is loaded
type Source interface {
	Read() (*ChangeSet, error)
	Write(set *ChangeSet) error
	Watch() (Watcher, error)
	String() string
}

// ChangeSet represents a set of changes from a source
type ChangeSet struct {
	Data      []byte
	CheckSum  string
	Format    string
	Source    string
	Timestamp time.Time
}

type Watcher interface {
	Next() (*ChangeSet, error)
	Stop() error
}
