package flag

import (
	"errors"
	"flag"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"github.com/imdario/mergo"
	"strings"
	"time"
)

type flagSrc struct {
	opts source.Options
}

func NewSource(opts ...source.Option) source.Source {
	return &flagSrc{opts: source.NewOptions(opts...)}
}

func (fs *flagSrc) Read() (*source.ChangeSet, error) {
	if !flag.Parsed() {
		return nil, errors.New("flags not parsed")
	}

	var changes map[string]interface{}
	visitFn := func(f *flag.Flag) {
		name := strings.ToLower(f.Name)
		keys := strings.FieldsFunc(name, split)
		reverse(keys)

		tmp := make(map[string]interface{})
		for i, k := range keys {
			if i == 0 {
				tmp[k] = f.Value
				continue
			}
			tmp = map[string]interface{}{k: tmp}
		}

		_ = mergo.Map(&changes, tmp)
		return
	}

	unset, ok := fs.opts.Context.Value(includeUnsetKey{}).(bool)
	if ok && unset {
		flag.VisitAll(visitFn)
	} else {
		flag.Visit(visitFn)
	}
	b, err := fs.opts.Encoder.Encode(changes)
	if err != nil {
		return nil, err
	}

	cs := &source.ChangeSet{
		Format:    fs.opts.Encoder.String(),
		Data:      b,
		Timestamp: time.Now(),
		Source:    fs.String(),
	}
	cs.CheckSum = cs.Sum()
	return cs, nil
}

func (f *flagSrc) Write(set *source.ChangeSet) error {
	return nil
}

func (f *flagSrc) Watch() (source.Watcher, error) {
	return source.NewNoopWatcher()
}

func (f *flagSrc) String() string {
	return "flagSrc"
}

func split(r rune) bool {
	return r == '-' || r == '_'
}
func reverse(s []string) {
	for i := len(s)/2 - 1; i >= 0; i-- {
		j := len(s) - 1 - i
		s[i], s[j] = s[j], s[i]
	}
}
