package flag

import (
	"encoding/json"
	"flag"
	"testing"
)

var (
	dbUser = flag.String("database-user", "default", "db user")
	dbHost = flag.String("database-host", "", "db host")
	dbPwd  = flag.String("database-password", "", "db pwd")
)

func initTestFlags() {
	flag.Set("database-host", "localhost")
	flag.Set("database-password", "some-password")
	flag.Parse()
}

func TestFlagSrc_Read(t *testing.T) {
	initTestFlags()

	source := NewSource()
	c, err := source.Read()
	if err != nil {
		t.Error(err)
	}

	var actual map[string]interface{}
	if err := json.Unmarshal(c.Data, &actual); err != nil {
		t.Error(err)
	}

	actualDB := actual["database"].(map[string]interface{})
	if actualDB["host"] != *dbHost {
		t.Errorf("expected %v got %v", *dbHost, actualDB["host"])
	}
	if actualDB["password"] != *dbPwd {
		t.Errorf("expected %v got %v", *dbPwd, actualDB["password"])
	}
	if actualDB["user"] != nil {
		t.Errorf("expected %v got %v", *dbUser, actualDB["user"])
	}
}

func TestFlagSrc_ReadAll(t *testing.T) {
	initTestFlags()
	source := NewSource(IncludeUnset(true))
	c, err := source.Read()
	if err != nil {
		t.Error(err)
	}

	var actual map[string]interface{}
	if err := json.Unmarshal(c.Data, &actual); err != nil {
		t.Error(err)
	}

	actualDB := actual["database"].(map[string]interface{})

	if actualDB["user"] != *dbUser {
		t.Errorf("expected %v got %v", *dbUser, actualDB["user"])
	}
}
