package file

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
)

type filePathKey struct {
}

// WithPath set the path to file
func WithPath(p string) source.Option {
	return func(o *source.Options) {
		if o.Context == nil {
			o.Context = context.Background()
		}
		o.Context = context.WithValue(o.Context, filePathKey{}, p)
	}
}
