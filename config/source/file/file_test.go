package file_test

import (
	"fmt"
	"gitee.com/pearl-zz/go-manage-system-core/config/source/file"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestFile(t *testing.T) {
	data := []byte(`{"foo":"bar"}`)
	path := filepath.Join(os.TempDir(), fmt.Sprintf("file.%d", time.Now().UnixNano()))
	fh, err := os.Create(path)
	if err != nil {
		t.Error(err)
	}
	defer func() {
		fh.Close()
		os.Remove(path)
	}()

	_, err = fh.Write(data)
	if err != nil {
		t.Error(err)
	}

	f := file.NewSource(file.WithPath(path))
	c, err := f.Read()
	if err != nil {
		t.Error(err)
	}

	if string(c.Data) != string(data) {
		t.Error("data from file does not match")
	}
}
