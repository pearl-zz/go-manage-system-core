package file

import (
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"io/ioutil"
	"os"
)

var (
	DefaultPath string
)

type file struct {
	path string
	opts source.Options
}

func NewSource(opts ...source.Option) source.Source {
	options := source.NewOptions(opts...)
	path := DefaultPath
	f, ok := options.Context.Value(filePathKey{}).(string)
	if ok {
		path = f
	}

	return &file{
		path: path,
		opts: options,
	}
}

func (f *file) Read() (*source.ChangeSet, error) {
	open, err := os.Open(f.path)
	if err != nil {
		return nil, err
	}

	defer open.Close()
	b, err := ioutil.ReadAll(open)
	if err != nil {
		return nil, err
	}
	info, err := open.Stat()
	if err != nil {
		return nil, err
	}

	cs := &source.ChangeSet{
		Data:      b,
		Format:    format(f.path, f.opts.Encoder),
		Source:    f.String(),
		Timestamp: info.ModTime(),
	}
	cs.CheckSum = cs.Sum()
	return cs, nil
}

func (f *file) Write(set *source.ChangeSet) error {
	return nil
}

func (f *file) Watch() (source.Watcher, error) {
	if _, err := os.Stat(f.path); err != nil {
		return nil, err
	}
	return newWatcher(f)
}

func (f *file) String() string {
	return "file"
}
