package file

import (
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"testing"
)

func TestFormat(t *testing.T) {
	opts := source.NewOptions()
	e := opts.Encoder

	testCases := []struct {
		path     string
		expected string
	}{
		{"/foo/bar.json", "json"},
		{"/foo/bar.yaml", "yaml"},
	}

	for _, d := range testCases {
		actual := format(d.path, e)
		if actual != d.expected {
			t.Fatalf("%s: expected %s got %s", d.path, d.expected, actual)
		}
	}
}
