package source

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder"
	"gitee.com/pearl-zz/go-manage-system-core/config/encoder/json"
)

type Options struct {
	Encoder encoder.Encoder
	Context context.Context
}

type Option func(options *Options)

func NewOptions(opts ...Option) Options {
	options := Options{
		Encoder: json.NewEncoder(),
		Context: context.Background(),
	}

	for _, o := range opts {
		o(&options)
	}
	return options
}

func WithEncoder(e encoder.Encoder) Option {
	return func(o *Options) {
		o.Encoder = e
	}
}
