package config

import (
	"context"
	"gitee.com/pearl-zz/go-manage-system-core/config/loader"
	"gitee.com/pearl-zz/go-manage-system-core/config/reader"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
)

type Options struct {
	Loader  loader.Loader
	Reader  reader.Reader
	Source  []source.Source
	Entity  Entity
	Context context.Context
}

type Option func(o *Options)

func WithLoader(l loader.Loader) Option {
	return func(o *Options) {
		o.Loader = l
	}
}

func WithReader(r reader.Reader) Option {
	return func(o *Options) {
		o.Reader = r
	}
}

func WithSource(s source.Source) Option {
	return func(o *Options) {
		o.Source = append(o.Source, s)
	}
}

func WithEntity(e Entity) Option {
	return func(o *Options) {
		o.Entity = e
	}
}
