package config

import (
	"fmt"
	"gitee.com/pearl-zz/go-manage-system-core/config/source"
	"gitee.com/pearl-zz/go-manage-system-core/config/source/env"
	"gitee.com/pearl-zz/go-manage-system-core/config/source/file"
	"gitee.com/pearl-zz/go-manage-system-core/config/source/memory"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
	"time"
)

func TestConfigLoadWithGoodFile(t *testing.T) {
	fh := createFile(t)
	path := fh.Name()
	defer func() {
		fh.Close()
		os.Remove(path)
	}()

	// Create new config
	conf, err := NewConfig()
	if err != nil {
		t.Fatalf("Expected no error but got %v", err)
	}

	// Load file source
	err = conf.Load(file.NewSource(file.WithPath(path)))
	if err != nil {
		t.Fatalf("Expected no error but got %v", err)
	}
}

func TestConfigLoadWithInvalidFile(t *testing.T) {
	fh := createFile(t)
	path := fh.Name()
	defer func() {
		fh.Close()
		os.Remove(path)
	}()

	// Create new config
	conf, err := NewConfig()
	if err != nil {
		t.Fatalf("Expected no error but got %v", err)
	}

	err = conf.Load(file.NewSource(
		file.WithPath(path),
		file.WithPath("/i/json.json"),
	))
	if err == nil {
		t.Fatal("Expected error but none!")
	}
	if !strings.Contains(fmt.Sprintf("%v", err), "/i/json.json") {
		t.Fatalf("Expected error to contain the unexisting file but got %v", err)
	}
}

func TestConfigMerge(t *testing.T) {
	content := `{
  "amqp": {
    "host": "rabbit.platform",
    "port": 80
  },
  "handler": {
    "exchange": "springCloudBus"
  }		
}`
	fh := createFileWithContent(t, content)
	path := fh.Name()
	defer func() {
		fh.Close()
		os.Remove(path)
	}()

	os.Setenv("AMQP_HOST", "rabbit.testing.com")

	conf, err := NewConfig()
	if err != nil {
		t.Fatalf("Expected no error but got %v", err)
	}
	err = conf.Load(
		file.NewSource(
			file.WithPath(path),
		),
		env.NewSource(),
	)
	if err != nil {
		t.Fatalf("Expected no error but got %v", err)
	}

	actualHost := conf.Get("amqp", "host").String("backup")
	if actualHost != "rabbit.testing.com" {
		t.Fatalf("Expected %v but got %v", "rabbit.testing.com", actualHost)
	}
}

func TestConfigWatcherDirtyOverride(t *testing.T) {
	n := runtime.GOMAXPROCS(0)
	defer runtime.GOMAXPROCS(n)

	runtime.GOMAXPROCS(1)
	l := 100
	ss := make([]source.Source, l, l)

	for i := 0; i < l; i++ {
		ss[i] = memory.NewSource(memory.WithJSON([]byte(fmt.Sprintf(`{"key%d": "val%d"}`, i, i))))
	}

	conf, _ := NewConfig()

	for _, s := range ss {
		_ = conf.Load(s)
	}
	runtime.Gosched()

	for i, _ := range ss {
		k := fmt.Sprintf("key%d", i)
		v := fmt.Sprintf("val%d", i)
		equalS(t, conf.Get(k).String(""), v)
	}
}

func createFile(t *testing.T) *os.File {
	data := `{"foo": "bar"}`
	return createFileWithContent(t, data)
}

func createFileWithContent(t *testing.T, content string) *os.File {
	data := []byte(content)
	path := filepath.Join(os.TempDir(), fmt.Sprintf("file.%d", time.Now().UnixNano()))
	file, err := os.Create(path)
	if err != nil {
		t.Error(err)
	}

	_, err = file.Write(data)
	if err != nil {
		t.Error(err)
	}
	return file
}
func equalS(t *testing.T, actual, expect string) {
	if actual != expect {
		t.Errorf("Expected %s but got %s", actual, expect)
	}
}
