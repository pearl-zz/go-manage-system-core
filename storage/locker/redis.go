package locker

import (
	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v7"
	"time"
)

type Redis struct {
	client *redis.Client
	mutex  *redislock.Client
}

// NewRedis 初始化Locker
func NewRedis(c *redis.Client) *Redis {
	return &Redis{client: c}
}

func (r *Redis) String() string {
	return "redis"
}

func (r *Redis) Lock(key string, ttl int64, options *redislock.Options) (*redislock.Lock, error) {
	if r.mutex == nil {
		r.mutex = redislock.New(r.client)
	}
	return r.mutex.Obtain(key, time.Duration(ttl)*time.Second, options)
}
