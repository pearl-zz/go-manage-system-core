package cache

import (
	"github.com/go-redis/redis/v7"
	"time"
)

type Redis struct {
	client *redis.Client
}

func NewRedis(client *redis.Client, options *redis.Options) (*Redis, error) {
	if client == nil {
		client = redis.NewClient(options)
	}
	r := &Redis{client: client}
	err := r.connect()
	if err != nil {
		return nil, err
	}

	return r, nil
}

// GetClient 暴露client
func (r *Redis) GetClient() *redis.Client {
	return r.client
}

// connect ping测试
func (r *Redis) connect() error {
	_, err := r.client.Ping().Result()
	return err
}

func (r *Redis) String() string {
	return "redis"
}

func (r *Redis) Get(key string) (string, error) {
	return r.client.Get(key).Result()
}

func (r *Redis) Set(key string, val interface{}, expire int) error {
	return r.client.Set(key, val, time.Duration(expire)*time.Second).Err()
}

func (r *Redis) Del(key string) error {
	return r.client.Del(key).Err()
}

func (r *Redis) HashGet(hk, key string) (string, error) {
	return r.client.HGet(hk, key).Result()
}

func (r *Redis) HashDel(hk, key string) error {
	return r.client.HDel(hk, key).Err()
}

func (r *Redis) Increase(key string) error {
	return r.client.Incr(key).Err()
}

func (r *Redis) Decrease(key string) error {
	return r.client.Decr(key).Err()
}

func (r *Redis) Expire(key string, dur time.Duration) error {
	return r.client.Expire(key, dur).Err()
}
