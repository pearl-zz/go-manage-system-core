package cache

import (
	"github.com/go-redis/redis/v7"
	"sync"
	"testing"
	"time"
)

func TestRedis_Get(t *testing.T) {
	type fields struct {
		items   *sync.Map
		queue   *sync.Map
		wait    sync.WaitGroup
		mutex   sync.RWMutex
		PoolNum uint
	}
	type args struct {
		key    string
		value  string
		expire int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			"test01",
			fields{},
			args{
				key:    "test",
				value:  "test",
				expire: 10,
			},
			"test",
			false,
		}, {
			"test02",
			fields{},
			args{
				key:    "test",
				value:  "test1",
				expire: 1,
			},
			"",
			true,
		},
	}
	redis, err := NewRedis(nil, &redis.Options{Username: "root", Password: "", Addr: "127.0.0.1:6379"})
	if err != nil {
		t.Error(err)
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if err := redis.Set(test.args.key, test.args.value, test.args.expire); err != nil {
				t.Errorf("Set() error = %v", err)
				return
			}

			time.Sleep(2 * time.Second)
			got, err := redis.Get(test.args.key)
			if (err != nil) != test.wantErr {
				t.Errorf("Get() error = %v, wantErr = %v", err, test.wantErr)
				return
			}

			if got != test.want {
				t.Errorf("Get() got = %v, want = %v", err, test.want)
			}
		})
	}
}
