package storage

import (
	"github.com/bsm/redislock"
	"time"
)

const (
	PrefixKey = "__host"
)

type AdapterCache interface {
	String() string
	Get(key string) (string, error)
	Set(key string, val interface{}, expire int) error
	Del(key string) error
	HashGet(hk, key string) (string, error)
	HashDel(hk, key string) error
	Increase(key string) error
	Decrease(key string) error
	Expire(key string, dur time.Duration) error
}

type Messager interface {
	SetID(string)
	SetStream(string)
	SetValues(map[string]interface{})
	SetPrefix(string)
	GetID() string
	GetStream() string
	GetValues() map[string]interface{}
	GetPrefix() string
}

type ConsumerFunc func(Messager) error

type AdapterQueue interface {
	String() string
	Append(message Messager) error
	Register(name string, f ConsumerFunc)
	Run()
	Shutdown()
}

type AdapterLocker interface {
	String() string
	Lock(key string, ttl int64, options *redislock.Options) (*redislock.Lock, error)
}
