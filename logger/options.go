package logger

import (
	"context"
	"io"
)

type Options struct {
	Level           Level                  // The logging level the logger should log at. default is `InfoLevel`
	Fields          map[string]interface{} // fields to always be logged
	Output          io.Writer              // It's common to set this to a file, or leave it default which is `os.Stderr`
	CallerSkipCount int                    // Caller skip frame count for file:line info
	Context         context.Context        // Alternative Option
	Name            string                 // logger name
}

type Option func(options *Options)

func WithLevel(level Level) Option {
	return func(o *Options) {
		o.Level = level
	}
}

func WithFields(fields map[string]interface{}) Option {
	return func(o *Options) {
		o.Fields = fields
	}
}

func WithOutput(output io.Writer) Option {
	return func(o *Options) {
		o.Output = output
	}
}

func WithCallerSkipCount(callerSkipCount int) Option {
	return func(o *Options) {
		o.CallerSkipCount = callerSkipCount
	}
}

func WithName(name string) Option {
	return func(o *Options) {
		o.Name = name
	}
}

func SetOption(k, v interface{}) Option {
	return func(o *Options) {
		if o.Context == nil {
			o.Context = context.Background()
		}
		o.Context = context.WithValue(o.Context, k, v)
	}
}
