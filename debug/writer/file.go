package writer

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// timeFormat 时间格式 用于文件名称格式
const timeFormat = "2006-01-02"

// FileWriter 结构体
type FileWriter struct {
	File         *os.File
	FileNameFunc func(*FileWriter) string
	Num          uint
	Opts         Options
	Input        chan []byte
}

// NewFileWriter 实例化FileWriter，支持大文件分割
func NewFileWriter(opts ...Option) (*FileWriter, error) {
	p := &FileWriter{
		Opts: setDefault(),
	}

	for _, o := range opts {
		o(&p.Opts)
	}

	var fileName string
	var err error
	for {
		fileName = p.getFileName()
		_, err := os.Stat(fileName)
		if err != nil {
			if os.IsNotExist(err) {
				if p.Num > 0 {
					p.Num--
					fileName = p.getFileName()
				}

				// 文件不存在
				break
			}

			// 存在，但是报错了
			return nil, err
		}

		p.Num++
		if p.Opts.Cap == 0 {
			break
		}
	}

	p.File, err = os.OpenFile(fileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE|os.O_SYNC, 0600)
	if err != nil {
		return nil, err
	}
	p.Input = make(chan []byte, 100)
	go p.write()
	return p, nil
}

// 获取log文件名，目前以日期格式命名 eg:2006-01-02.log
func (p *FileWriter) getFileName() string {
	if p.FileNameFunc != nil {
		return p.FileNameFunc(p)
	}
	if p.Opts.Cap == 0 {
		return filepath.Join(
			p.Opts.Path,
			fmt.Sprintf("%s.%s", time.Now().Format(timeFormat), p.Opts.Suffix))
	}

	return filepath.Join(
		p.Opts.Path,
		fmt.Sprintf("%s-[%d].[%s]", time.Now().Format(timeFormat), p.Num, p.Opts.Suffix))
}

func (p *FileWriter) write() {
	for {
		select {
		case d := <-p.Input:
			_, err := p.File.Write(d)
			if err != nil {
				log.Printf("write file failed,%s\n", err.Error())
			}
			p.checkFile()
		}
	}
}

func (p *FileWriter) checkFile() {
	info, _ := p.File.Stat()
	if strings.Index(p.File.Name(), time.Now().Format(timeFormat)) < 0 ||
		(p.Opts.Cap > 0 && uint(info.Size()) > p.Opts.Cap) {
		// 生成新文件
		if uint(info.Size()) > p.Opts.Cap {
			p.Num++
		} else {
			p.Num = 0
		}
		fileName := p.getFileName()
		_ = p.File.Close()
		p.File, _ = os.OpenFile(fileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE|os.O_SYNC, 0600)
	}
}

// Write 写入方法
func (p *FileWriter) Write(data []byte) (n int, err error) {
	if p == nil {
		return 0, errors.New("logFileWriter is nil")
	}
	if p.File == nil {
		return 0, errors.New("file not opened")
	}
	n = len(data)
	go func() {
		p.Input <- data
	}()

	return n, nil
}
