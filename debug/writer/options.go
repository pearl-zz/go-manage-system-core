package writer

type Options struct {
	Path   string
	Suffix string
	Cap    uint
}

func setDefault() Options {
	return Options{
		Path:   "/tmp/go-manage-system",
		Suffix: "log",
		Cap:    0,
	}
}

// Option set options
type Option func(*Options)

func WithPath(s string) Option {
	return func(o *Options) {
		o.Path = s
	}
}

func WithSuffix(s string) Option {
	return func(o *Options) {
		o.Suffix = s
	}
}
func WithCap(c uint) Option {
	return func(o *Options) {
		o.Cap = c
	}
}
