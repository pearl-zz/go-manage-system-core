package log

import (
	"encoding/json"
	"fmt"
	"time"
)

var (
	DefaultSize   = 256
	DefaultFormat = TextFormat
)

// Options are logger options
type Options struct {
	Name   string
	Size   int
	Format FormatFunc
}

// Option used by the logger
type Option func(*Options)

// Name of the log
func Name(n string) Option {
	return func(o *Options) {
		o.Name = n
	}
}

// Size sets the size of the ring buffer
func Size(s int) Option {
	return func(o *Options) {
		o.Size = s
	}
}

func Format(f FormatFunc) Option {
	return func(o *Options) {
		o.Format = f
	}
}

// DefaultOptions returns default options
func DefaultOptions() Options {
	return Options{
		Size:   DefaultSize,
		Format: DefaultFormat,
	}
}

// ReadOptions for querying the logs
type ReadOptions struct {
	Since  time.Time
	Count  int
	Stream bool
}

// ReadOption used for reading the logs
type ReadOption func(*ReadOptions)

func Since(s time.Time) ReadOption {
	return func(o *ReadOptions) {
		o.Since = s
	}
}

func Count(c int) ReadOption {
	return func(o *ReadOptions) {
		o.Count = c
	}
}

type FormatFunc func(Record) string

func TextFormat(r Record) string {
	t := r.Timestamp.Format("2006-01-02 15:04:05")
	return fmt.Sprintf("%s %v", t, r.Message)
}

func JSONFormat(r Record) string {
	b, _ := json.Marshal(r)
	return string(b) + " "
}
