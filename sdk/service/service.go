package service

import (
	"fmt"
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"gorm.io/gorm"
)

type Service struct {
	Orm   *gorm.DB
	Msg   string
	MsgID string
	Log   *logger.Helper
	Error error
}

func (s *Service) AddError(err error) error {
	if s.Error == nil {
		s.Error = err
	} else if s.Error != nil {
		s.Error = fmt.Errorf("%v,%w", s.Error, err)
	}
	return s.Error
}
