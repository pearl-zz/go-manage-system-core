package response

type Response struct {
	// 数据集
	RequestId string `protobuf:"bytes,1,opt,name=requestId,proto3" json:"requestId,omitempty"`
	Code      int32  `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
	Msg       string `protobuf:"bytes,3,opt,name=msg,proto3" json:"msg,omitempty"`
	Status    string `protobuf:"bytes,4,opt,name=status,proto3" json:"status,omitempty"`
}

type response struct {
	Response
	Data interface{} `json:"data"`
}

type Page struct {
	Count     int `json:"count"`
	PageIndex int `json:"pageIndex"`
	PageSize  int `json:"pageSize"`
}

type page struct {
	Page
	List interface{} `json:"list"`
}

func (r *response) SetCode(code int32) {
	r.Code = code
}

func (r *response) SetTraceID(id string) {
	r.RequestId = id
}

func (r *response) SetMsg(msg string) {
	r.Msg = msg
}

func (r *response) SetData(data interface{}) {
	r.Data = data
}

func (r *response) SetSuccess(success bool) {
	if !success {
		r.Status = "error"
	}
}

func (r response) Clone() Responses {
	return &r
}
