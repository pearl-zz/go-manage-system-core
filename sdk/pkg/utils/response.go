package utils

import (
	"net/http"
	"time"
)

type APIException struct {
	Code      int         `json:"code"`
	Success   bool        `json:"success"`
	Msg       string      `json:"msg"`
	Timestamp int64       `json:"timestamp"`
	Result    interface{} `json:"result"`
}

func NewAPIException(code int, success bool, msg string, data interface{}) *APIException {
	return &APIException{Code: code, Success: success, Msg: msg, Timestamp: time.Now().Unix(), Result: data}
}

func (e *APIException) Error() string {
	return e.Msg
}

// ServerError 500 错误处理
func ServerError() *APIException {
	return NewAPIException(http.StatusInternalServerError, false, http.StatusText(http.StatusInternalServerError), nil)
}

// NotFound 404 错误
func NotFound() *APIException {
	return NewAPIException(http.StatusNotFound, false, http.StatusText(http.StatusNotFound), nil)
}

// UnknownError 未知错误
func UnknownError(message string) *APIException {
	return NewAPIException(http.StatusForbidden, false, message, nil)
}

// ParameterError 参数错误
func ParameterError(message string) *APIException {
	return NewAPIException(http.StatusBadRequest, false, message, nil)
}

// AuthError 授权错误
func AuthError(message string) *APIException {
	return NewAPIException(http.StatusBadRequest, false, message, nil)
}

// ResponseJson 200
func ResponseJson(message string, data interface{}, success bool) *APIException {
	return NewAPIException(http.StatusOK, success, message, data)
}
