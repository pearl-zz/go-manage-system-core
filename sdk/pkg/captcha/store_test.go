package captcha

import (
	"fmt"
	"gitee.com/pearl-zz/go-manage-system-core/storage"
	"gitee.com/pearl-zz/go-manage-system-core/storage/cache"
	"github.com/mojocn/base64Captcha"
	"math/rand"
	"testing"
)

var _expiration = 6000

func getStore(_ *testing.T) storage.AdapterCache {
	return cache.NewMemory()
}

func TestSetGet(t *testing.T) {
	s := NewCacheStore(getStore(t), _expiration)
	key := "captcha key"
	val := "random-string"
	s.Set(key, val)
	result := s.Get(key, false)
	if result != val {
		t.Errorf("saved %v, getDigits returned got %v", val, result)
	}
}

func TestGetClear(t *testing.T) {
	s := NewCacheStore(getStore(t), _expiration)
	key := "captcha key"
	val := "random-string"
	s.Set(key, val)
	result := s.Get(key, true)
	if val != result {
		t.Errorf("saved %v, getDigits returned got %v", val, result)
	}

	result = s.Get(key, false)
	if result != "" {
		t.Errorf("saved %v, getDigits returned got %v", val, result)
	}
}

func BenchmarkSetCollect(b *testing.B) {
	store := cache.NewMemory()
	b.StopTimer()
	val := "captcha key"
	s := NewCacheStore(store, -1)
	keys := make([]string, 1000)

	for i := range keys {
		keys[i] = fmt.Sprintf("%d", rand.Int63())
	}

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		for j := 0; j < 1000; j++ {
			s.Set(keys[j], val)
		}
	}
}

func TestNewCacheStore(t *testing.T) {
	type args struct {
		store      storage.AdapterCache
		expiration int
	}
	tests := []struct {
		name string
		args args
		want base64Captcha.Store
	}{
		{"", args{getStore(t), 36000}, nil},
		{"", args{getStore(t), 180000}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCacheStore(tt.args.store, tt.args.expiration); got == nil {
				t.Errorf("NewMemoryStore() = %v, want %v", got, tt.want)
			}
		})
	}
}
