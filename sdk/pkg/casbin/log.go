package mycasbin

import (
	"gitee.com/pearl-zz/go-manage-system-core/logger"
	"sync/atomic"
)

type Logger struct {
	enable int32
}

func (l *Logger) EnableLog(enable bool) {
	i := 0
	if enable {
		i = 1
	}
	atomic.StoreInt32(&(l.enable), int32(i))
}

func (l *Logger) IsEnabled() bool {
	return atomic.LoadInt32(&(l.enable)) != 0
}

func (l *Logger) LogModel(model [][]string) {
	var str string
	for i := range model {
		for j := range model[i] {
			str += " " + model[i][j]
		}
		str += "\n"
	}
	logger.DefaultLogger.Log(logger.InfoLevel, str)
}

func (l *Logger) LogEnforce(matcher string, request []interface{}, result bool, explains [][]string) {
	logger.DefaultLogger.Fields(map[string]interface{}{
		"matcher":  matcher,
		"request":  request,
		"result":   result,
		"explains": explains,
	}).Log(logger.InfoLevel, nil)
}

func (l *Logger) LogRole(roles []string) {
	logger.DefaultLogger.Fields(map[string]interface{}{
		"roles": roles,
	})
}

func (l *Logger) LogPolicy(policy map[string][][]string) {
	data := make(map[string]interface{}, len(policy))
	for k := range policy {
		data[k] = policy[k]
	}
	logger.DefaultLogger.Fields(data).Log(logger.InfoLevel, nil)
}
