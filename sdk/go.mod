module gitee.com/pearl-zz/go-manage-system-core/sdk

go 1.15

require (
	gitee.com/pearl-zz/go-manage-system-core v1.0.3
	gitee.com/pearl-zz/go-manage-system-core/plugins/logger/zap v1.0.3
	github.com/bsm/redislock v0.5.0
	github.com/bytedance/go-tagexpr/v2 v2.9.1
	github.com/casbin/casbin/v2 v2.40.6
	github.com/chanxuehong/wechat v0.0.0-20211009063332-41a5c6d8b38b
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/go-admin-team/gorm-adapter/v3 v3.2.1-0.20210902112335-4148cb356a24
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis/v7 v7.4.1
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.4.2
	github.com/mojocn/base64Captcha v1.3.1
	github.com/nsqio/go-nsq v1.1.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/robinjoseph08/redisqueue/v2 v2.1.0
	github.com/shamsher31/goimgext v1.0.0
	github.com/slok/go-http-metrics v0.10.0
	github.com/smartystreets/goconvey v1.7.2
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	gorm.io/gorm v1.21.11
)

replace (
	gitee.com/pearl-zz/go-manage-system-core => ../
	gitee.com/pearl-zz/go-manage-system-core/plugins/logger/zap => ../plugins/logger/zap
)
