package config

import (
	"github.com/go-redis/redis/v7"
)

var _redis *redis.Client

func GetRedisClient() *redis.Client {
	return _redis
}

func SetRedisClient(c *redis.Client) {
	if _redis != nil && _redis != c {
		_redis.Shutdown()
	}
	_redis = c
}

type RedisConnectionOptions struct {
	Network    string `json:"network" yaml:"network"`
	Addr       string `json:"addr" yaml:"addr"`
	Username   string `json:"username" yaml:"username"`
	Password   string `json:"password" yaml:"password"`
	DB         int    `json:"db" yaml:"db"`
	PoolSize   int    `json:"pool_size" yaml:"pool_size"`
	Tls        *Tls   `json:"tls" yaml:"tls"`
	MaxRetries int    `json:"max_retries" yaml:"max_retries"`
}

func (e RedisConnectionOptions) GetRedisOptions() (*redis.Options, error) {
	r := &redis.Options{
		Network:    e.Network,
		Addr:       e.Addr,
		Username:   e.Username,
		Password:   e.Password,
		DB:         e.DB,
		MaxRetries: e.MaxRetries,
		PoolSize:   e.PoolSize,
	}
	var err error
	r.TLSConfig, err = getTLS(e.Tls)
	return r, err
}
