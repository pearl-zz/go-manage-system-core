package config

import (
	"gitee.com/pearl-zz/go-manage-system-core/storage"
	"gitee.com/pearl-zz/go-manage-system-core/storage/locker"
	"github.com/go-redis/redis/v7"
)

type Locker struct {
	Redis *RedisConnectionOptions
}

var LockerConfig = new(Locker)

func (e Locker) Empty() bool {
	return e.Redis == nil
}

// Setup 顺序 redis -> 其他 -> memory
func (e Locker) Setup() (storage.AdapterLocker, error) {
	if e.Redis != nil {
		client := GetRedisClient()
		if client == nil {
			options, err := e.Redis.GetRedisOptions()
			if err != nil {
				return nil, err
			}

			client = redis.NewClient(options)
			_redis = client
		}
		return locker.NewRedis(client), nil
	}
	return nil, nil
}
