package runtime

import "gitee.com/pearl-zz/go-manage-system-core/storage"

type Queue struct {
	prefix string
	queue  storage.AdapterQueue
}

// NewQueue 实例化
func NewQueue(prefix string, queue storage.AdapterQueue) storage.AdapterQueue {
	return &Queue{prefix: prefix, queue: queue}
}

func (e *Queue) String() string {
	return e.queue.String()
}

// Append 添加数据到生产者
func (e *Queue) Append(message storage.Messager) error {
	values := message.GetValues()
	if values == nil {
		values = make(map[string]interface{})
	}
	values[storage.PrefixKey] = e.prefix
	return e.queue.Append(message)
}

// Register 注册消费者
func (e *Queue) Register(name string, f storage.ConsumerFunc) {
	e.queue.Register(name, f)
}

func (e *Queue) Run() {
	e.queue.Run()
}

func (e *Queue) Shutdown() {
	if e.queue != nil {
		e.queue.Shutdown()
	}
}
