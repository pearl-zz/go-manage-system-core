package runtime

import (
	"gitee.com/pearl-zz/go-manage-system-core/storage"
	"github.com/bsm/redislock"
)

type Locker struct {
	prefix string
	locker storage.AdapterLocker
}

//NewLocker 实例化
func NewLocker(prefix string, locker storage.AdapterLocker) storage.AdapterLocker {
	return &Locker{prefix: prefix, locker: locker}
}

func (l *Locker) String() string {
	return l.locker.String()
}

// Lock 返回分布式锁对象
func (l *Locker) Lock(key string, ttl int64, options *redislock.Options) (*redislock.Lock, error) {
	return l.locker.Lock(l.prefix+intervalTenant+key, ttl, options)
}
